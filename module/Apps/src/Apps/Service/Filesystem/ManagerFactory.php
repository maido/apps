<?php

namespace Apps\Service\Filesystem;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use League\Flysystem\MountManager;

class ManagerFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $manager = new MountManager();
        return $manager;
    }

}
