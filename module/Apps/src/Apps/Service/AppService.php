<?php

namespace Apps\Service;

use Apps\Entity\App;
use Apps\Entity\Page;
use Zend\Http\Request;
use Zend\Mvc\Router\SimpleRouteStack;
use Zend\Mvc\Router\Http\TreeRouteStack;
use Doctrine\Common\Collections\Collection;

class AppService extends AbstractService
{

    /**
     * Get master domain
     * 
     * @return string
     * @throws \RuntimeException
     */
    public function getMasterDomain()
    {
        $config = $this->getServiceLocator()->get('config');

        if (!isset($config['apps']['domain'])) {
            throw new \RuntimeException('Missing [apps][domain] key in config.');
        }

        return $config['apps']['domain'];
    }

    /**
     * Get app from request
     * 
     * @param Request $request
     * @return \Apps\Entity\App|null
     */
    public function getAppFromRequest(Request $request)
    {
        $master = $this->getMasterDomain();
        $domain = $request->getUri()->getHost();

        $repository = $this->getAppRepository();

        if ($domain === $master) {
            $criteria = ['handle' => null];
        } elseif (strlen($domain) > strlen($master) && substr($domain, -strlen($master)) === $master) {
            $name = substr($domain, 0, - strlen($master) - 1);
            $criteria = ['handle' => $name];
        } else {
            $criteria = ['domain' => $domain];
        }

        $app = $repository->findOneBy($criteria);

        return $app;
    }

    /**
     * Get router
     * 
     * @param App $app
     * @param SimpleRouteStack $router
     * @return SimpleRouteStack
     */
    public function getRouter(App $app, SimpleRouteStack $router = null)
    {
        $routes = $router ? $router->getRoutes()->toArray() : [];

        $home = $this->getPageService()->getHome($app);
        $options = [
            'routes' => array_merge_recursive($routes, $this->getPageService()->getRouteConfig($home)),
        ];

        $router = TreeRouteStack::factory($options);

        return $router;
    }

    /**
     * Save app to database
     * 
     * @param App $app
     * @param boolean $flush
     * @return App
     */
    public function save(App $app, $flush = false)
    {
        $this->getEntityManager()->persist($app);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $app;
    }

    /**
     * Create a new app
     * 
     * @param App|array $app
     * @param boolean $flush
     * @return App
     * @throws \InvalidArgumentException
     */
    public function create($app, $flush = true)
    {
        if (is_array($app)) {
            $app = $this->getHydrator()->hydrate($app, new App());
        } elseif (!$app instanceof App) {
            throw new \InvalidArgumentException('Argument must be an array or an instance of App entity.');
        }

        return $this->save($app, $flush);
    }

}
