<?php

namespace Apps\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class PdoResourceFactory implements FactoryInterface
{

    /**
     * Create service
     * 
     * @param ServiceLocatorInterface $serviceLocator
     * @return \PDO
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $db \Zend\Db\Adapter\Adapter */
        $db = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $pdo = $db->getDriver()->getConnection()->getResource();
        
        if (!$pdo instanceof \PDO) {
            throw new \RuntimeException('A PDO instance is expected.');
        }
        
        return $pdo;
    }

}
