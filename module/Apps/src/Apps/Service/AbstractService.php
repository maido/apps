<?php

namespace Apps\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as ObjectHydrator;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Stdlib\Hydrator\NamingStrategy\UnderscoreNamingStrategy;
use Zend\Stdlib\Hydrator\NamingStrategy\NamingStrategyInterface;

/**
 * @method \Apps\Service\AppService getAppService()
 * @method \Apps\Service\PageService getPageService()
 * @method \Doctrine\ORM\EntityRepository getAppRepository()
 * @method \Doctrine\ORM\EntityRepository getPageRepository()
 */
abstract class AbstractService implements ServiceLocatorAwareInterface
{

    /**
     * Implement service locator aware interface
     */
    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    /**
     * @var \Zend\Stdlib\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * @var \Zend\Stdlib\Hydrator\NamingStrategy\NamingStrategyInterface
     */
    protected $namingStrategy;

    /**
     * Get entity manager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }

    /**
     * Magic call method
     * 
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \RuntimeException
     */
    public function __call($name, $arguments)
    {
        if (substr($name, 0, 3) === 'get' && substr($name, -7) === 'Service') {
            $service = 'Apps\Service\\' . substr($name, 3, -7);
            if ($this->getServiceLocator()->has($service)) {
                return $this->getServiceLocator()->get($service);
            }
        } elseif (substr($name, 0, 3) === 'get' && substr($name, -10) === 'Repository') {
            $class = 'Apps\Entity\\' . substr($name, 3, -10);
            if (class_exists($class)) {
                return $this->getEntityManager()->getRepository($class);
            }
        }

        throw new \RuntimeException('Invalid method: ' . $name);
    }

    /**
     * Set hydrator
     * 
     * @param HydratorInterface $hydrator
     * @return \Apps\Service\AbstractService
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
        return $this;
    }

    /**
     * Get hydrator
     * 
     * @return \Zend\Stdlib\Hydrator\HydratorInterface
     */
    public function getHydrator()
    {
        if ($this->hydrator === null) {
            $this->hydrator = new ObjectHydrator($this->getEntityManager());
            $this->hydrator->setNamingStrategy($this->getNamingStrategy());
        }

        return $this->hydrator;
    }

    /**
     * Set naming strategy
     * 
     * @param NamingStrategyInterface $namingStrategy
     * @return \Apps\Service\AbstractService
     */
    public function setNamingStrategy(NamingStrategyInterface $namingStrategy)
    {
        $this->namingStrategy = $namingStrategy;
        return $this;
    }

    /**
     * Get naming strategy
     * 
     * @return \Zend\Stdlib\Hydrator\NamingStrategy\NamingStrategyInterface
     */
    public function getNamingStrategy()
    {
        if ($this->namingStrategy === null) {
            $this->namingStrategy = new UnderscoreNamingStrategy();
        }
        return $this->namingStrategy;
    }

}
