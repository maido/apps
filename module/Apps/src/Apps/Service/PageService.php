<?php

namespace Apps\Service;

use Apps\Entity\Page;
use Apps\Entity\App;

class PageService extends AbstractService
{

    /**
     * Get home
     * 
     * @param App $app
     * @return \Apps\Entity\Page|null
     */
    public function getHome(App $app)
    {
        $home = null;

        /* @var $page \Apps\Entity\Page */
        foreach ($app->getPages() as $page) {
            if ($page->getHandle() === null) {
                $home = $page;
                break;
            }
        }

        return $home;
    }

    /**
     * Get route config
     * 
     * @param Page $page
     * @param boolean $childRoutes
     * @return array
     */
    public function getRouteConfig(Page $page, $childRoutes = true)
    {
        $handle = $page->getHandle();
        $name = $handle === null ? 'home' : $handle;

        $config = [
            $name => [
                'type' => 'literal',
                'options' => [
                    'route' => '/' . $handle,
                    'defaults' => [
                        'controller' => 'Apps\Controller\Page',
                        'action' => 'index',
                        'id' => $page->getId(),
                    ],
                ],
            ]
        ];
        
        if ($childRoutes) {
            $routes = [];
            foreach ($page->getChildren() as $child) {
                $routes = array_merge($routes, $this->getRouteConfig($child, $childRoutes));
            }
            $config[$name]['may_terminate'] = true;
            $config[$name]['child_routes'] = $routes;
        }
        
        return $config;
    }

}
