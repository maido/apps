<?php

namespace Apps\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * @codeCoverageIgnore
 */
class Init extends Command implements ServiceLocatorAwareInterface
{

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected function configure()
    {
        $this->setName('apps:init')
                ->setDescription('Initialize database with base content.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $entityManager \Doctrine\ORM\EntityManager */
        $entityManager = $this->getHelper('em')->getEntityManager();        
        
        /* @var $appService \Apps\Service\AppService */
        $appService = $this->getServiceLocator()->get('Apps\Service\App');
        
        $data = [
            'name' => 'Maido Apps',
            'pages' => [
                [
                    'title' => 'Maido Apps',
                    'description' => 'Web apps made easy.',
                ],
            ],
        ];
        $app = $appService->create($data);
    }

}
