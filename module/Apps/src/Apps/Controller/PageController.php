<?php

namespace Apps\Controller;

class PageController extends AbstractController
{

    /**
     * Index action
     * 
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function indexAction()
    {
        $id = $this->params()->fromRoute('id');
        $page = $this->getPageService()->getPageRepository()->find($id);

        $response = $this->getResponse();
        return $response;
    }

}
