<?php

namespace Apps\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * @method \Apps\Service\AppService getAppService()
 * @method \Apps\Service\PageService getPageService()
 */
class AbstractController extends AbstractActionController
{

    /**
     * Magic call method
     * 
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \RuntimeException
     */
    public function __call($name, $arguments)
    {
        if (substr($name, 0, 3) === 'get' && substr($name, -7) === 'Service') {
            $service = 'Apps\Service\\' . substr($name, 3, -7);
            if ($this->getServiceLocator()->has($service)) {
                return $this->getServiceLocator()->get($service);
            }
        }

        return parent::__call($name, $arguments);
    }

}
