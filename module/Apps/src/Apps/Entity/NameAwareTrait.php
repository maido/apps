<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;

trait NameAwareTrait
{

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * Set name
     * 
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
