<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;

trait TitleAwareTrait
{

    /**
     * @var string
     * @ORM\Column()
     */
    private $title;

    /**
     * Set title
     * 
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

}
