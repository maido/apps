<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;

trait OrderingAwareTrait
{

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordering;

    /**
     * Set ordering
     * 
     * @param int $ordering
     * @return self
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;
        return $this;
    }

    /**
     * Get ordering
     * 
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

}
