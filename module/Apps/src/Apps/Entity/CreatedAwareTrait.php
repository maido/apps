<?php

namespace Apps\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait CreatedAwareTrait
{

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime") 
     */
    private $created;

    /**
     * Set created
     * 
     * @param \DateTime $created
     * @return self
     */
    public function setCreated(DateTime $created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     * 
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created on pre persist
     * 
     * @ORM\PrePersist
     * @return void
     */
    public function setCreatedOnPrePersist()
    {
        $this->created = new DateTime();
    }

    /**
     * Set created on pre Update
     * 
     * @ORM\PreUpdate
     * @return void
     */
    public function setCreatedOnPreUpdate()
    {
        // do nothing
    }

}
