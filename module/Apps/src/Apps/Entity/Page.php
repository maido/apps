<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(indexes={
 *   @ORM\Index(name="handle", columns={"handle"})
 * })
 */
class Page
{

    /**
     * Common properties through traits
     */
    use IdAutoAwareTrait,
        CreatedAwareTrait,
        UpdatedAwareTrait,
        TitleAwareTrait,
        DescriptionAwareTrait,
        OrderingAwareTrait;

    /**
     * @var \Apps\Entity\App
     * @ORM\ManyToOne(targetEntity="App", inversedBy="pages")
     */
    private $app;

    /**
     * @var \Apps\Entity\Page
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     */
    private $parent;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $children;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $handle;

    /**
     * Add child
     *
     * @param \Apps\Entity\Page $child
     *
     * @return Page
     */
    public function addChild(\Apps\Entity\Page $child)
    {
        $this->children->add($child);
        $child->setParent($this);

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Apps\Entity\Page $child
     *
     * @return Page
     */
    public function removeChild(\Apps\Entity\Page $child)
    {
        $this->children->removeElement($child);
        $child->setParent(null);

        return $this;
    }

    /**
     * Add children
     * 
     * @param Collection $children
     * @return \Apps\Entity\Page
     */
    public function addChildren(Collection $children)
    {
        foreach ($children as $child) {
            $this->addChild($child);
        }

        return $this;
    }

    /**
     * Remove children
     * 
     * @param Collection $children
     * @return \Apps\Entity\Page
     */
    public function removeChildren(Collection $children)
    {
        foreach ($children as $child) {
            $this->removeChild($child);
        }

        return $this;
    }

    /**
     * Get path
     * 
     * @return string
     */
    public function getPath()
    {
        $path = '/' . $this->handle;

        if ($this->parent && $this->parent->getHandle()) {
            $path = $this->parent->getPath() . $path;
        }

        return $path;
    }

    /* DOCTRINE GENERATED CODE */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set handle
     *
     * @param string $handle
     *
     * @return Page
     */
    public function setHandle($handle)
    {
        $this->handle = $handle;

        return $this;
    }

    /**
     * Get handle
     *
     * @return string
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Set app
     *
     * @param \Apps\Entity\App $app
     *
     * @return Page
     */
    public function setApp(\Apps\Entity\App $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \Apps\Entity\App
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set parent
     *
     * @param \Apps\Entity\Page $parent
     *
     * @return Page
     */
    public function setParent(\Apps\Entity\Page $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Apps\Entity\Page
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

}
