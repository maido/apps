<?php

namespace Apps\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait UpdatedAwareTrait
{

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime") 
     */
    private $updated;

    /**
     * Set updated
     * 
     * @param \DateTime $updated
     * @return self
     */
    public function setUpdated(DateTime $updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get updated
     * 
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated on pre persist
     * 
     * @ORM\PrePersist
     * @return void
     */
    public function setUpdatedOnPrePersist()
    {
        $this->updated = new DateTime();
    }

    /**
     * Set updated on pre Update
     * 
     * @ORM\PreUpdate
     * @return void
     */
    public function setUpdatedOnPreUpdate()
    {
        $this->updated = new DateTime();
    }

}
