<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;

trait DescriptionAwareTrait
{

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $description;

    /**
     * Set description
     * 
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}
