<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;

trait IdAutoAwareTrait
{

    /**
     * @var int
     * 
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
