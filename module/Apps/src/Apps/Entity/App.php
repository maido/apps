<?php

namespace Apps\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class App
{

    /**
     * Common properties through traits
     */
    use IdAutoAwareTrait,
        CreatedAwareTrait,
        UpdatedAwareTrait,
        NameAwareTrait;

    /**
     * @var string
     * @ORM\Column(unique=true, nullable=true)
     */
    private $handle;

    /**
     * @var string
     * @ORM\Column(unique=true, nullable=true)
     */
    private $domain;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Page", mappedBy="app", fetch="EAGER", cascade={"persist", "remove"})
     */
    private $pages;

    /**
     * Add page
     *
     * @param Page $page
     *
     * @return App
     */
    public function addPage(Page $page)
    {
        $this->pages->add($page);
        $page->setApp($this);

        return $this;
    }

    /**
     * Remove page
     *
     * @param Page $page
     *
     * @return App
     */
    public function removePage(Page $page)
    {
        $this->pages->removeElement($page);
        $page->setApp(null);

        return $this;
    }

    /**
     * Add pages
     * 
     * @param Collection $pages
     * @return \Apps\Entity\App
     */
    public function addPages(Collection $pages)
    {
        foreach ($pages as $page) {
            $this->addPage($page);
        }

        return $this;
    }

    /**
     * Remove pages
     * 
     * @param Collection $pages
     * @return \Apps\Entity\App
     */
    public function removePages(Collection $pages)
    {
        foreach ($pages as $page) {
            $this->removePage($page);
        }

        return $this;
    }

    /* DOCTRINE GENERATED CODE */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set handle
     *
     * @param string $handle
     *
     * @return App
     */
    public function setHandle($handle)
    {
        $this->handle = $handle;

        return $this;
    }

    /**
     * Get handle
     *
     * @return string
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return App
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

}
