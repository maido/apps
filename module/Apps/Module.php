<?php

namespace Apps;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\EventManager\EventInterface;
use Apps\Console\Command\Init;

class Module
{

    /**
     * On bootstrap
     * 
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_ROUTE, function(MvcEvent $e) {
            $serviceManager = $e->getApplication()->getServiceManager();
            $request = $serviceManager->get('request');

            if (!$request instanceof \Zend\Http\Request) {
                return;
            }

            /* @var $appService \Apps\Service\AppService */
            $appService = $serviceManager->get('Apps\Service\App');

            if (!$app = $appService->getAppFromRequest($request)) {
                $response = $e->getResponse();
                $response->setStatusCode(403)->setContent('<h1>Forbidden</h1>');
                return $response;
            }

            $router = $appService->getRouter($app);
            $e->setRouter($router);
        }, 100);
    }

    /**
     * Get config
     * 
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get autoloader config
     * 
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function init(ModuleManagerInterface $manager)
    {
        $events = $manager->getEventManager()->getSharedManager();
        //$sm = $manager->getEvent()->getParam('ServiceManager');

        $events->attach('doctrine', 'loadCli.post', function(EventInterface $e) {
            $sm = $e->getParam('ServiceManager');

            /* @var $cli \Symfony\Component\Console\Application */
            $cli = $e->getTarget();

            $initCommand = new Init();
            $initCommand->setServiceLocator($sm);
            $cli->add($initCommand);
        });
    }

}
