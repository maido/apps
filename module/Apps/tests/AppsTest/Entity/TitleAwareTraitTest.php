<?php

namespace AppsTest\Entity;

class TitleAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetTitleProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\TitleAwareTrait');
        $title = 'Just a title';

        $result = $mock->setTitle($title);

        $this->assertSame($mock, $result);
        $this->assertEquals($title, $result->getTitle());
    }

}
