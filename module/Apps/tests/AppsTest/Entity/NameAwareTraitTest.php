<?php

namespace AppsTest\Entity;

class NameAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetNameProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\NameAwareTrait');
        $name = 'Just a name';
        
        $result = $mock->setName($name);
        
        $this->assertSame($mock, $result);
        $this->assertEquals($name, $result->getName());
    }

}
