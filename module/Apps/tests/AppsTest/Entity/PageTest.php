<?php

namespace AppsTest\Entity;

use Apps\Entity\App;
use Apps\Entity\Page;
use Doctrine\Common\Collections\ArrayCollection;

class PageTest extends \PHPUnit_Framework_TestCase
{

    public function testPageClassExists()
    {
        $this->assertTrue(class_exists('Apps\Entity\Page'));
    }

    public function testPageEntityCanSetAndGetHandle()
    {
        $page = new Page();
        $handle = 'handle';

        $this->assertNull($page->getHandle());
        $this->assertInstanceOf('Apps\Entity\Page', $page->setHandle($handle));
        $this->assertEquals($handle, $page->getHandle());
    }

    public function testPageEntityCanSetAndGetApp()
    {
        $page = new Page();
        $app = new App();

        $this->assertNull($page->getApp());
        $this->assertInstanceOf('Apps\Entity\Page', $page->setApp($app));
        $this->assertSame($app, $page->getApp());
    }

    public function testPageEntityCanSetAndGetParent()
    {
        $page = new Page();
        $parent = new Page();

        $this->assertNull($page->getParent());
        $this->assertInstanceOf('Apps\Entity\Page', $page->setParent($parent));
        $this->assertSame($parent, $page->getParent());
    }

    public function testPageEntityCanGetAllChildren()
    {
        $page = new Page();
        $this->assertInstanceOf(ArrayCollection::class, $page->getChildren());
    }

    public function testPageEntityCanAddAndRemoveASingleChild()
    {
        $page = new Page();
        $child = new Page();

        $this->assertCount(0, $page->getChildren());
        $page->addChild($child);
        $this->assertCount(1, $page->getChildren());
        $page->removeChild($child);
        $this->assertCount(0, $page->getChildren());
    }

    public function testPageEntityCanAddAndRemoveMultipleChildren()
    {
        $page = new Page();

        $children = new ArrayCollection([new Page(), new Page(), new Page()]);

        $this->assertCount(0, $page->getChildren());
        $page->addChildren($children);
        $this->assertCount($children->count(), $page->getChildren());
        $page->removeChildren($children);
        $this->assertCount(0, $page->getChildren());
    }

    public function testPageEntityCanGetPath()
    {
        $page1 = new Page();

        $page2 = new Page();
        $page2->setHandle('page2')->setParent($page1);

        $page3 = new Page();
        $page3->setHandle('page3')->setParent($page2);

        $this->assertEquals('/', $page1->getPath());
        $this->assertEquals('/page2', $page2->getPath());
        $this->assertEquals('/page2/page3', $page3->getPath());
    }
}
