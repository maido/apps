<?php

namespace AppsTest\Entity;

class UpdatedAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetUpdatedProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\UpdatedAwareTrait');
        $created = new \DateTime();

        $this->assertSame($mock, $mock->setUpdated($created));
        $this->assertSame($created, $mock->getUpdated());
    }

    public function testTraitSetsUpdatedPropertyOnPrePersist()
    {
        $mock = $this->getMockForTrait('Apps\Entity\UpdatedAwareTrait');

        $this->assertNull($mock->getUpdated());

        $mock->setUpdatedOnPrePersist();
        $this->assertInstanceOf('DateTime', $mock->getUpdated());
    }

    public function testTraitSetsUpdatedPropertyOnPreUpdate()
    {
        $mock = $this->getMockForTrait('Apps\Entity\UpdatedAwareTrait');

        $this->assertNull($mock->getUpdated());

        $mock->setUpdatedOnPreUpdate();
        $this->assertInstanceOf('DateTime', $mock->getUpdated());
    }

}
