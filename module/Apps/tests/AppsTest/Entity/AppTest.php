<?php

namespace AppsTest\Entity;

use Apps\Entity\App;
use Apps\Entity\Page;
use Doctrine\Common\Collections\ArrayCollection;

class AppTest extends \PHPUnit_Framework_TestCase
{

    public function testAppClassExists()
    {
        $this->assertTrue(class_exists('Apps\Entity\App'));
    }

    public function testAppEntityCanSetAndGetHandle()
    {
        $app = new App();
        $handle = 'handle';

        $this->assertNull($app->getHandle());
        $this->assertInstanceOf('Apps\Entity\App', $app->setHandle($handle));
        $this->assertEquals($handle, $app->getHandle());
    }

    public function testAppEntityCanSetAndGetDomain()
    {
        $app = new App();
        $domain = 'domain';

        $this->assertNull($app->getDomain());
        $this->assertInstanceOf('Apps\Entity\App', $app->setDomain($domain));
        $this->assertEquals($domain, $app->getDomain());
    }

    public function testAppEntityCanGetAllPages()
    {
        $app = new App();
        $this->assertInstanceOf(ArrayCollection::class, $app->getPages());
    }

    public function testAppEntityCanAddAndRemoveASinglePage()
    {
        $app = new App();
        $page = new Page();

        $this->assertCount(0, $app->getPages());
        $app->addPage($page);
        $this->assertCount(1, $app->getPages());
        $app->removePage($page);
        $this->assertCount(0, $app->getPages());
    }

    public function testAppEntityCanAddAndRemoveMultiplePages()
    {
        $app = new App();
        
        $pages = new ArrayCollection([new Page(), new Page(), new Page()]);

        $this->assertCount(0, $app->getPages());
        $app->addPages($pages);
        $this->assertCount($pages->count(), $app->getPages());
        $app->removePages($pages);
        $this->assertCount(0, $app->getPages());
    }

}
