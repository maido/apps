<?php

namespace AppsTest\Entity;

class DescriptionAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetDescriptionProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\DescriptionAwareTrait');
        $description = 'Just a description';

        $result = $mock->setDescription($description);

        $this->assertSame($mock, $result);
        $this->assertEquals($description, $result->getDescription());
    }

}
