<?php

namespace AppsTest\Entity;

class IdAutoAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanGetIdProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\IdAutoAwareTrait');
        $this->assertNull($mock->getId());
    }

}
