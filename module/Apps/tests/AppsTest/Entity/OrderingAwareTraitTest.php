<?php

namespace AppsTest\Entity;

class OrderingAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetOrderingProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\OrderingAwareTrait');
        $ordering = 6;

        $result = $mock->setOrdering($ordering);

        $this->assertSame($mock, $result);
        $this->assertEquals($ordering, $result->getOrdering());
    }

}
