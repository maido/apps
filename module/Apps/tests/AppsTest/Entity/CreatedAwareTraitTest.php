<?php

namespace AppsTest\Entity;

class CreatedAwareTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testTraitCanSetAndGetCreatedProperty()
    {
        $mock = $this->getMockForTrait('Apps\Entity\CreatedAwareTrait');
        $created = new \DateTime();

        $this->assertSame($mock, $mock->setCreated($created));
        $this->assertSame($created, $mock->getCreated());
    }

    public function testTraitSetsCreatedPropertyOnPrePersist()
    {
        $mock = $this->getMockForTrait('Apps\Entity\CreatedAwareTrait');

        $this->assertNull($mock->getCreated());

        $mock->setCreatedOnPrePersist();
        $this->assertInstanceOf('DateTime', $mock->getCreated());
    }

    public function testTraitSetsCreatedPropertyOnPreUpdate()
    {
        $mock = $this->getMockForTrait('Apps\Entity\CreatedAwareTrait');

        $this->assertNull($mock->getCreated());

        $mock->setCreatedOnPreUpdate();
        $this->assertNull($mock->getCreated());
    }

}
