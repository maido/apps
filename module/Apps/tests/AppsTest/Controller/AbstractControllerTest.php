<?php

namespace AppsTest\Controller;

class AbstractControllerTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = include __DIR__ . '/../../../config/module.config.php';
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('config', $config);

        parent::setUp();
    }

    public function testAbstractControllerClassExtendsAbstractActionController()
    {
        $instance = $this->getMockForAbstractClass(\Apps\Controller\AbstractController::class);
        $this->assertInstanceOf(\Zend\Mvc\Controller\AbstractActionController::class, $instance);
    }

    public function testMagicMethodCallCanReturnAValidService()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $instance = $this->getMockForAbstractClass(\Apps\Controller\AbstractController::class);
        $instance->setServiceLocator($serviceLocator);

        $this->assertInstanceOf('Apps\Service\AppService', $instance->getAppService());
    }

    public function testMagicMethodCallThrowsExceptionWhenMethodNameIsNotValid()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $instance = $this->getMockForAbstractClass(\Apps\Controller\AbstractController::class);
        $instance->setServiceLocator($serviceLocator);

        $this->setExpectedException('Zend\ServiceManager\Exception\ServiceNotFoundException');
        $instance->getInvalidService();
    }

}
