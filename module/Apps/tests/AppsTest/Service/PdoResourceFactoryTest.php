<?php

namespace AppsTest\Service;

use Apps\Service\PdoResourceFactory;

class PdoResourceFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testPdoResourceFactoryImplementsFactoryInterface()
    {
        $pdoResourceFactory = new PdoResourceFactory();
        $this->assertInstanceOf('Zend\ServiceManager\FactoryInterface', $pdoResourceFactory);
    }

    public function testCreateServiceReturnsAPdoInstance()
    {
        $expected = new PDOMock();

        $pdoResourceFactory = new PdoResourceFactory();

        /* @var $serviceLocator \Zend\ServiceManager\ServiceLocatorInterface */
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Zend\Db\Adapter\Adapter', $this->getDbAdaterMock($expected));

        $pdo = $pdoResourceFactory->createService($serviceLocator);

        $this->assertInstanceOf('Pdo', $pdo);
    }

    public function testCreateServiceThrowsExceptionWhenResourceIsNotAPdoInstance()
    {
        $this->setExpectedException('RuntimeException');

        $pdoResourceFactory = new PdoResourceFactory();

        /* @var $serviceLocator \Zend\ServiceManager\ServiceLocatorInterface */
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Zend\Db\Adapter\Adapter', $this->getDbAdaterMock(null));

        $pdo = $pdoResourceFactory->createService($serviceLocator);
    }

    public function testAppsServicePdoResourceIsAService()
    {
        $config = include __DIR__ . '/../../../config/module.config.php';
        $this->assertTrue(isset($config['service_manager']['factories']['Apps\Service\PdoResource']));
    }

    /**
     * @return \Zend\Db\Adapter\Adapter
     */
    protected function getDbAdaterMock($resource = null)
    {
        $connection = $this->getMockBuilder('Zend\Db\Adapter\Driver\Pdo\Connection')
                ->disableOriginalConstructor()
                ->getMock();
        $connection->method('getResource')
                ->willReturn($resource);

        $driver = $this->getMockBuilder('Zend\Db\Adapter\Driver\Pdo\Pdo')
                ->disableOriginalConstructor()
                ->getMock();
        $driver->method('getConnection')
                ->willReturn($connection);

        $adapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                ->disableOriginalConstructor()
                ->getMock();
        $adapter->method('getDriver')
                ->willReturn($driver);

        return $adapter;
    }

}

class PDOMock extends \PDO
{

    public function __construct()
    {
        // do nothing
    }

}
