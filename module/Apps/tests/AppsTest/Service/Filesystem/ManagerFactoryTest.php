<?php

namespace AppsTest\Service\Filesystem;

use Apps\Service\Filesystem\ManagerFactory;

class ManagerFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testManagerFactoryImplementsFactoryInterface()
    {
        $managerFactory = new ManagerFactory();
        $this->assertInstanceOf('Zend\ServiceManager\FactoryInterface', $managerFactory);
    }

    public function testCreateServiceMethodReturnsAnInstanceOfMountManager()
    {
        $managerFactory = new ManagerFactory();
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $manager = $managerFactory->createService($serviceLocator);
        $this->assertInstanceOf('League\Flysystem\MountManager', $manager);
    }

    public function testAppsServiceFilesystemManagerIsAService()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $this->assertTrue($serviceLocator->has('Apps\Service\Filesystem\Manager'));
    }

}
