<?php

namespace AppsTest\Service;

use Apps\Service\PageService;
use Apps\Entity\Page;
use Apps\Entity\App;

class PageServiceTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = include __DIR__ . '/../../../config/module.config.php';
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('config', $config);

        parent::setUp();
    }

    public function testPageServiceClassExists()
    {
        $this->assertTrue(class_exists('Apps\Service\PageService'));
    }

    public function testPageServiceClassExtendsAbstractService()
    {
        $class = new PageService();
        $this->assertInstanceOf('Apps\Service\AbstractService', $class);
    }

    public function testAppsServicePageIsAService()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $this->assertTrue($serviceLocator->has('Apps\Service\Page'));
        $this->assertInstanceOf('Apps\Service\PageService', $serviceLocator->get('Apps\Service\Page'));
    }

    public function testGetHomeReturnsHomePageFromApp()
    {
        $service = new PageService();

        $home = new Page();
        $app = new App();
        $app->addPage($home);

        $this->assertSame($home, $service->getHome($app));
    }

    public function testGetRouteConfigReturnsArrayWithRouterConfiguration()
    {
        $service = new PageService();

        $home = new Page();

        $config = $service->getRouteConfig($home, true);
        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('home', $config);
        $this->assertArrayHasKey('type', $config['home']);
        $this->assertArrayHasKey('options', $config['home']);
        $this->assertArrayHasKey('route', $config['home']['options']);
        $this->assertArrayHasKey('defaults', $config['home']['options']);
        $this->assertArrayHasKey('controller', $config['home']['options']['defaults']);
        $this->assertArrayHasKey('action', $config['home']['options']['defaults']);
        $this->assertArrayHasKey('id', $config['home']['options']['defaults']);

        $child1 = new Page();
        $child1->setHandle('child1');
        $home->addChild($child1);

        $config = $service->getRouteConfig($home, true);
        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('home', $config);
        $this->assertArrayHasKey('child_routes', $config['home']);
        $this->assertArrayHasKey('child1', $config['home']['child_routes']);

        $child2 = new Page();
        $child2->setHandle('child2');
        $home->addChild($child2);

        $config = $service->getRouteConfig($home, true);
        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('home', $config);
        $this->assertArrayHasKey('child_routes', $config['home']);
        $this->assertArrayHasKey('child1', $config['home']['child_routes']);
        $this->assertArrayHasKey('child2', $config['home']['child_routes']);

        $child3 = new Page();
        $child3->setHandle('child3');
        $child1->addChild($child3);

        $config = $service->getRouteConfig($home, true);
        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('home', $config);
        $this->assertArrayHasKey('child_routes', $config['home']);
        $this->assertArrayHasKey('child1', $config['home']['child_routes']);
        $this->assertArrayHasKey('child_routes', $config['home']['child_routes']['child1']);
        $this->assertArrayHasKey('child3', $config['home']['child_routes']['child1']['child_routes']);
    }

}
