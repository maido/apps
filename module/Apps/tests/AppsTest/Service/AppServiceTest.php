<?php

namespace AppsTest\Service;

use Apps\Service\AppService;
use Apps\Entity\App;
use Apps\Entity\Page;
use Zend\Http\Request;
use Zend\Uri\Http as Uri;

class AppServiceTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = include __DIR__ . '/../../../config/module.config.php';
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('config', $config);

        parent::setUp();
    }

    public function testAppServiceClassExists()
    {
        $this->assertTrue(class_exists('Apps\Service\AppService'));
    }

    public function testAppServiceClassExtendsAbstractService()
    {
        $class = new AppService();
        $this->assertInstanceOf('Apps\Service\AbstractService', $class);
    }

    public function testAppsServiceAppIsAService()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $this->assertTrue($serviceLocator->has('Apps\Service\App'));
        $this->assertInstanceOf('Apps\Service\AppService', $serviceLocator->get('Apps\Service\App'));
    }

    public function testGetMasterDomainReturnsNameOfMasterDomain()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = $serviceLocator->get('config');

        $service = $serviceLocator->get('Apps\Service\App');
        $domain = $config['apps']['domain'];

        $this->assertEquals($domain, $service->getMasterDomain());
    }

    public function testGetMasterDomainThrowsExceptionIfAppsDomainKeyIsNotSet()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = $serviceLocator->get('config');

        unset($config['apps']['domain']);
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('config', $config);

        $service = $serviceLocator->get('Apps\Service\App');

        $this->setExpectedException('RuntimeException');
        $service->getMasterDomain();
    }

    /**
     * @dataProvider requestProvider
     * @param Request $request
     */
    public function testGetAppFromRequestReturnsAppEntity(Request $request)
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $repository = $this->getMockBuilder('Doctrine\ORM\EntityRepository')
                ->disableOriginalConstructor()
                ->getMock();
        $repository->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnCallback(function() {
                            return new App();
                        }));

        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')
                ->getMock();
        $entityManager->expects($this->any())
                ->method('getRepository')
                ->with('Apps\Entity\App')
                ->will($this->returnValue($repository));

        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $service = $serviceLocator->get('Apps\Service\App');
        $this->assertInstanceOf('Apps\Entity\App', $service->getAppFromRequest($request));
    }

    public function requestProvider()
    {
        return [
            [(new Request())->setUri((new Uri())->setHost('apps.local'))],
            [(new Request())->setUri((new Uri())->setHost('test.apps.local'))],
            [(new Request())->setUri((new Uri())->setHost('example.org'))],
        ];
    }

    public function testGetRouterReturnsAnInstanceOfSimpleStackRoute()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $service = $serviceLocator->get('Apps\Service\App');

        $app = new App();
        $home = new Page();
        $app->addPage($home);

        $this->assertInstanceOf(\Zend\Mvc\Router\SimpleRouteStack::class, $service->getRouter($app));

        $config = [
            'routes' => [
                'home' => [
                    'type' => 'literal',
                    'options' => [
                        'route' => '/',
                        'defaults' => [
                            'controller' => 'Apps\Controller\Page',
                            'action' => 'index',
                        ]
                    ]
                ]
            ]
        ];
        $router = \Zend\Mvc\Router\Http\TreeRouteStack::factory($config);
        $this->assertInstanceOf(\Zend\Mvc\Router\SimpleRouteStack::class, $service->getRouter($app, $router));
    }

    protected function getEntityManagerMock(array $methods = [])
    {
        return $this->getMockBuilder(\Doctrine\ORM\EntityManager::class)
                        ->disableOriginalConstructor()
                        ->setMethods($methods)
                        ->getMock();
    }

    protected function getAppMock($id = null)
    {
        $app = $this->getMockBuilder(App::class)->getMock();
        $app->method('getId')
                ->will($this->returnValue($id));
        return $app;
    }

    public function testSaveCanPersistApp()
    {
        $app = new App();
        $app->setName('Maido Apps');

        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $entityManager = $this->getEntityManagerMock();

        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $service = new AppService();
        $service->setServiceLocator($serviceLocator);

        $this->assertSame($app, $service->save($app));
        $this->assertSame($app, $service->save($app, true));
    }

    public function testCreateCanCreateAppFromArray()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $entityManager = $this->getEntityManagerMock();
        $strategy = new \Zend\Stdlib\Hydrator\NamingStrategy\UnderscoreNamingStrategy();
        $hydrator = new \Zend\Stdlib\Hydrator\ClassMethods();
        $hydrator->setNamingStrategy($strategy);

        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $service = new AppService();
        $service->setServiceLocator($serviceLocator);
        $service->setHydrator($hydrator);

        $name = 'Maido Apps';
        $app = $service->create(['name' => $name]);

        $this->assertInstanceOf(App::class, $app);
        $this->assertEquals($name, $app->getName());
    }

    public function testCreateCanCreateAppFromAppInstance()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $entityManager = $this->getEntityManagerMock();

        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $service = new AppService();
        $service->setServiceLocator($serviceLocator);

        $name = 'Maido Apps';
        $app = new App();
        $app->setName($name);

        $this->assertInstanceOf(App::class, $service->create($app));
        $this->assertEquals($name, $service->create($app)->getName());
    }

    public function testCreateThrowsExceptionWhenArgumentIsNotValid()
    {
        $service = new AppService();

        $this->setExpectedException(\InvalidArgumentException::class);
        $service->create('app');
    }

}
