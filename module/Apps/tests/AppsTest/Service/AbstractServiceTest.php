<?php

namespace AppsTest\Service;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Stdlib\Hydrator\NamingStrategy\NamingStrategyInterface;

class AbstractServiceTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $config = include __DIR__ . '/../../../config/module.config.php';
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('config', $config);

        parent::setUp();
    }

    public function testAbstractServiceClassExists()
    {
        $this->assertTrue(class_exists('Apps\Service\AbstractService'));
    }

    public function testAbstractServiceClassImplementsServiceLocatorAwareInterface()
    {
        $class = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $this->assertInstanceOf('Zend\ServiceManager\ServiceLocatorAwareInterface', $class);
    }

    public function testGetEntityManagerReturnsDoctrineEntityManager()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $class = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $class->setServiceLocator($serviceLocator);

        $this->assertInstanceOf('Doctrine\ORM\EntityManagerInterface', $class->getEntityManager());
    }

    public function testMagicMethodCallCanReturnAValidService()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $class = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $class->setServiceLocator($serviceLocator);

        $this->assertInstanceOf('Apps\Service\AppService', $class->getAppService());
    }

    public function testMagicMethodCallCanReturnAValidRepository()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();

        $repository = $this->getMockBuilder('Doctrine\ORM\EntityRepository')
                ->disableOriginalConstructor()
                ->getMock();

        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $entityManager->method('getRepository')
                ->with('Apps\Entity\App')
                ->will($this->returnValue($repository));

        $serviceLocator->setAllowOverride(true);
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $class = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $class->setServiceLocator($serviceLocator);

        $this->assertInstanceOf('Doctrine\ORM\EntityRepository', $class->getAppRepository());
    }

    public function testMagicMethodCallThrowsExceptionWhenMethodNameIsNotValid()
    {
        $this->setExpectedException('RuntimeException');
        $class = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $class->getInvalidRepository();
    }

    public function testServiceCanSetAndGetHydratorInstance()
    {
        $serviceLocator = \AppsTest\Bootstrap::getServiceManager();
        $serviceLocator->setAllowOverride(true);
        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $serviceLocator->setService('Doctrine\ORM\EntityManager', $entityManager);

        $hydrator = $this->getMockBuilder(HydratorInterface::class)->getMock();
        $service = $this->getMockForAbstractClass('Apps\Service\AbstractService');
        $service->setServiceLocator($serviceLocator);

        $this->assertInstanceOf(HydratorInterface::class, $service->getHydrator());
        $service->setHydrator($hydrator);
        $this->assertSame($hydrator, $service->getHydrator());
    }

    public function testServiceCanSetAndGetNamingStrategyInstance()
    {
        $strategy = $this->getMockBuilder(NamingStrategyInterface::class)->getMock();
        $service = $this->getMockForAbstractClass('Apps\Service\AbstractService');

        $this->assertInstanceOf(NamingStrategyInterface::class, $service->getNamingStrategy());
        $service->setNamingStrategy($strategy);
        $this->assertSame($strategy, $service->getNamingStrategy());
    }

}
