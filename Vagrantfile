VAGRANTFILE_API_VERSION = '2'

@script = <<SCRIPT
DOCUMENT_ROOT="/var/www/public"
apt-get update

# install MySQL
DBUSER="root"
DBPASS="Passw0rd"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASS"
apt-get install -y mysql-server

# install Apache and PHP
apt-get install -y apache2 git curl php5-cli php5-curl php5 php5-intl php5-json php5-gd php5-imagick php5-mysql php5-mcrypt php5-xdebug libapache2-mod-php5
php5enmod mcrypt

# enable xdebug and webgrind (optional)
#php5enmod xdebug
#echo "xdebug.profiler_enable_trigger = 1" >> /etc/php5/apache2/conf.d/20-xdebug.ini
#git clone https://github.com/jokkedk/webgrind.git /vagrant/public/webgrind

# create vhost script
echo "
<VirtualHost *:80>
    ServerName apps.local
    ServerAlias *.apps.local
    UseCanonicalName Off
    SetEnv APPLICATION_ENV development
    DocumentRoot $DOCUMENT_ROOT
    <Directory $DOCUMENT_ROOT>
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>
<VirtualHost *:443>
    ServerName apps.local
    ServerAlias *.apps.local
    UseCanonicalName Off
    SetEnv APPLICATION_ENV development
    DocumentRoot $DOCUMENT_ROOT
    <Directory $DOCUMENT_ROOT>
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
</VirtualHost>
" > /etc/apache2/sites-available/apps.local.conf

# Apache setup
a2enmod rewrite
a2enmod ssl
a2dissite 000-default
a2ensite apps.local
service apache2 restart

# application setup
cd /var/www
rm -rf html
curl -Ss https://getcomposer.org/installer | php
php composer.phar install --no-progress
cp config/autoload/local.php.dist config/autoload/local.php

echo "** Add '127.0.0.1 apps.local' to your /etc/hosts"
echo "** Visit http://apps.local:8085 or  https://apps.local:44333 in your browser for to view the application"
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = 'chef/ubuntu-14.04'
  config.vm.network "forwarded_port", guest: 80, host: 8085
  config.vm.network "forwarded_port", guest: 443, host: 44333
  config.vm.hostname = "apps.local"
  config.vm.synced_folder '.', '/var/www', id: "vagrant-root", :mount_options => ["dmode=777","fmode=666"]
  config.vm.provision 'shell', inline: @script

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end

end
